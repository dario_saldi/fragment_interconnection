package com.example.dariosaldi.fragment_interconnection_2;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


public class DetailFragment extends Fragment {

        private TextView detailView;

    public DetailFragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_detail, container, false);
    }

    @Override
    public void onStart() {
        super.onStart();
        Bundle args=getArguments();
        if (args!=null) {
            getData(args.getString("message"));
        }
    }

   public void getData (String message){
       detailView = (TextView)getActivity().findViewById(R.id.detailView);
       if (detailView!=null){
           detailView.setText("NAME: " + message);
       }else{
           openActivity();
       }

    }

    private void openActivity() {
        Intent open = new Intent(DetailFragment.this.getActivity(),MainActivity.class);
        DetailFragment.this.startActivity(open);
    }
}
