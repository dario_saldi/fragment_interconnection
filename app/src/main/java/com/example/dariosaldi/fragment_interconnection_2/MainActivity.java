package com.example.dariosaldi.fragment_interconnection_2;

/*
Passaggio di dati tra fragment: Immettendo i dati nel primo fragment viene passato il dato NOME al secondo fragment
[Funziona: gestito anche il tasto back]
 */

import android.content.res.Configuration;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;


public class MainActivity extends AppCompatActivity implements SendData {

    private FragmentManager fragmentManager;
    private FragmentTransaction fragmentTransaction;
    private MainFragment mainFragment;
    private DetailFragment detailFragment;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Controlliamo il layout ed evitiamo che i fragment si sovrappongano
        if (findViewById(R.id.mainContainer) != null) {
            if ((savedInstanceState != null)) {
                return;
            }
            openMainContainer();
        }
    }


    public void openMainContainer() {
        fragmentManager = getSupportFragmentManager();
        fragmentTransaction = fragmentManager.beginTransaction();
        mainFragment = new MainFragment();
        fragmentTransaction.add(R.id.mainContainer, mainFragment, "mainFragment");
        fragmentTransaction.commit();
    }

    @Override
    public void sendData(String message) {
        openDetailContent(message);
    }


        public void openDetailContent (String message){
            detailFragment = (DetailFragment) getSupportFragmentManager().findFragmentById(R.id.detail_fragment);
            detailFragment = new DetailFragment();

            Bundle args = new Bundle();
            args.putString("message", message);
            detailFragment.setArguments(args);

            fragmentManager = getSupportFragmentManager();  //ricreo il fragment manager
            fragmentTransaction = fragmentManager.beginTransaction();
            Configuration configuration = getResources().getConfiguration();
            if (configuration.orientation == Configuration.ORIENTATION_LANDSCAPE) {
                detailFragment.setArguments(args);
                fragmentTransaction.replace(R.id.detailContainer, detailFragment, "detailFragment");
            } else {
                fragmentTransaction.replace(R.id.mainContainer, detailFragment, "detailFragment");
                fragmentTransaction.addToBackStack("mainFragment");
            }
            fragmentTransaction.commit();
        }


        @Override
        public void onBackPressed () {
            if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
                getSupportFragmentManager().popBackStack();
            } else {
                super.onBackPressed();
            }
            getSupportFragmentManager().popBackStack("mainFragment", FragmentManager.POP_BACK_STACK_INCLUSIVE);
        }
    }

