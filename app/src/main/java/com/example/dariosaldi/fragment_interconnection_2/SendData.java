package com.example.dariosaldi.fragment_interconnection_2;


public interface SendData {
    public void sendData(String message);
}
